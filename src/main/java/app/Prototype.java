package app;

public interface Prototype {
    Prototype clone();
}
