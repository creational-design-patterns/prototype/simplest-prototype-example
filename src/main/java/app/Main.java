package app;

import java.time.LocalDate;

public class Main {

    // A Client Code example
    public static void main(String[] args) {
        Task originalTask = new Task("Create a Prototype design pattern example",
                LocalDate.of(2024, 2, 6));

        Task taskCopy = originalTask.clone();
        taskCopy.setDueOn(LocalDate.of(2024, 12, 31));

        System.out.println(originalTask);
        System.out.println(taskCopy);
    }
}
