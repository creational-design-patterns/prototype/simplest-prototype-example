package app;

import java.time.LocalDate;

public class Task implements Prototype {
    private String title;
    private LocalDate dueOn;

    public Task(String title, LocalDate dueOn) {
        this.title = title;
        this.dueOn = dueOn;
    }

    public String getTitle() {
        return title;
    }

    public LocalDate getDueOn() {
        return dueOn;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setDueOn(LocalDate dueOn) {
        this.dueOn = dueOn;
    }

    @Override
    public Task clone() {
        return new Task(this.title, this.dueOn);
    }

    @Override
    public String toString() {
        return "Task{" +
                "title='" + title + '\'' +
                ", dueOn=" + dueOn +
                '}';
    }
}
